// Synthesia
// Copyright (c)2007 Nicholas Piegdon
// See license.txt for license information

#ifndef __OS_GRAPHICS_H
#define __OS_GRAPHICS_H


#include "os.h"

#ifdef WIN32
#include <GL/gl.h>
#include <GL/glu.h>
#elif MACOS
#include <OpenGL/OpenGL.h>
#include <AGL/agl.h>
#include <AGL/gl.h>
#include <AGL/glu.h>
#elif LINUX
#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>
#include <GL/glx.h>
#endif



#endif
