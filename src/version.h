// Synthesia
// Copyright (c)2007 Nicholas Piegdon
// See license.txt for license information

#ifndef __SYNTHESIA_VERSION_H
#define __SYNTHESIA_VERSION_H

#include <string>

// See readme.txt for a list of what has changed between versions
static const std::wstring SynthesiaVersionString = L"0.6.1";

#endif
