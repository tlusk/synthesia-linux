// Synthesia
// Copyright (c)2007 Nicholas Piegdon
// See license.txt for license information

#include "CompatibleSystem.h"
#include "string_util.h"
#include "version.h"
#include "os.h"
#include "os_graphics.h"


namespace Compatible
{
   unsigned long GetMilliseconds()
   {
#ifdef WIN32
      return timeGetTime();
#elif MACOS
      timeval tv;
      gettimeofday(&tv, 0);
      return (tv.tv_sec * 1000) + (tv.tv_usec / 1000);
#elif LINUX
      struct timeb currentTime;
      ftime(&currentTime);
      return (currentTime.time * 1000) + currentTime.millitm;
#endif
   }


   void ShowError(const std::wstring &err)
   {
      const static std::wstring friendly_app_name = WSTRING(L"Synthesia " << SynthesiaVersionString);
      const static std::wstring message_box_title = WSTRING(friendly_app_name << L" Error");
      
#ifdef WIN32
      MessageBox(0, err.c_str(), message_box_title.c_str(), MB_ICONERROR);
#elif MACOS
      
      DialogRef dialog;
      DialogItemIndex item;

      // The cursor might have been hidden.
      ShowMouseCursor();

      CreateStandardAlert(kAlertStopAlert, MacStringFromWide(message_box_title).get(), MacStringFromWide(err).get(), 0, &dialog);
      RunStandardAlert(dialog, 0, &item);
      
#elif LINUX
      GtkWidget *dialog, *label;
                      
      // TODO: Not unicode
      std::string lmessage_box_title(message_box_title.begin(), message_box_title.end());
      std::string lerr(err.begin(), err.end());
      
      dialog = gtk_dialog_new_with_buttons (lmessage_box_title.c_str(), NULL, GTK_DIALOG_DESTROY_WITH_PARENT,
                                            GTK_STOCK_OK, GTK_RESPONSE_NONE, NULL);
      label = gtk_label_new (lerr.c_str());

      /* Ensure that the dialog box is destroyed when the user responds. */
      g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_main_quit), dialog);
      g_signal_connect_swapped (dialog, "response", G_CALLBACK (gtk_widget_destroy), dialog);
           
      /* Add the label, and show everything we've added to the dialog. */
      gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox), label);
      
      gtk_widget_show_all (dialog);
      
      gtk_main();
#endif
   }

   void HideMouseCursor()
   {
#ifdef WIN32
      ShowCursor(false);
#elif MACOS
      CGDisplayHideCursor(kCGDirectMainDisplay);
#endif
   }
   
   void ShowMouseCursor()
   {
#ifdef WIN32
      ShowCursor(true);
#elif MACOS
      CGDisplayShowCursor(kCGDirectMainDisplay);
#endif
   }


   int GetDisplayWidth()
   {
#ifdef WIN32
      return GetSystemMetrics(SM_CXSCREEN);
#elif LINUX
      const SDL_VideoInfo* videoInfo = SDL_GetVideoInfo( );
      if( videoInfo != NULL ) {
    	  return videoInfo->current_w;
      } else {
    	  return 0;
      }
#elif MACOS
      return int(CGDisplayBounds(kCGDirectMainDisplay).size.width);
#endif
   }

   int GetDisplayHeight()
   {
#ifdef WIN32
      return GetSystemMetrics(SM_CYSCREEN);
#elif LINUX
      const SDL_VideoInfo* videoInfo = SDL_GetVideoInfo( );
      if( videoInfo != NULL ) {
       	  return videoInfo->current_h;
      } else {
       	  return 0;
      }
#elif MACOS
      return int(CGDisplayBounds(kCGDirectMainDisplay).size.height);
#endif
   }


   void GracefulShutdown()
   {
#ifdef WIN32
      PostQuitMessage(0);
#elif LINUX
      SDL_Quit( );
      gtk_exit( 0 );
      exit( 0 );
#elif MACOS
      QuitApplicationEventLoop();
#endif
   }

}; // End namespace
